/**
 * Create two const for import discord.js lib
 */
const Discord = require("discord.js");
const bot = new Discord.Client();

/**
 * import File System (native module from node.js) for file manipulations
 */

const fs = require("fs");

/**
 * if the bot is launch, confirm in the console he is ready to be use
 */

bot.on("ready", () => console.log(`${bot.user.username} est prêt à être utilisé`));

/**
 * detect all messages in all the discord
 */

bot.on('message',msg=>{

    /**
     * reply 'python' when we say '!anaconda'
     */

    if(msg.content === '!anaconda'){
        msg.reply('Python');
    }
        else if(msg.content === '!repowest'){
            msg.reply('https://gitlab.com/W-mseg/west-bot');
        }
        else if(msg.content === '!veillecss'){
            msg.reply('https://github.com/W-mseg/veilledessincss');
        }
        else if(msg.content === '!vieux'){
            msg.reply("tu est trop jeune")
    }
});


/**
 * read the token.json where your token is
 */
bot.login(JSON.parse(fs.readFileSync("token.json")));